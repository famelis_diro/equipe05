package models;

import java.time.LocalDate;


public class ServiceConfirmationModel {
    private LocalDate actualDate;
    private String proId;
    private String memberId;
    private String serviceCode;
    private String comments;

    public ServiceConfirmationModel(LocalDate actualDate, String proId, String memberId, String serviceCode) {
        this.actualDate = actualDate;
        this.proId = proId;
        this.memberId = memberId;
        this.serviceCode = serviceCode;
        this.comments = "";
    }
}
