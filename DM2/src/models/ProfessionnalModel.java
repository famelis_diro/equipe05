package models;

import java.util.ArrayList;
import java.util.Date;

public class ProfessionnalModel extends PersonModel {

    public ArrayList<ServiceModel> services;

    public ProfessionnalModel(String lastName, String firstName, String adress, String city,
                              String telNumber, String favoriteWord, String age) {
        super(lastName, firstName, adress, city, telNumber, favoriteWord, age);
        this.role = "Professionnel";
    }

    public ProfessionnalModel(String lastName, String firstName, String adress, String city,
                              String telNumber, String favoriteWord, String age, String id) {
        super(lastName, firstName, adress, city, telNumber, favoriteWord, age, id);
        this.role = "Professionnel";
    }


}
