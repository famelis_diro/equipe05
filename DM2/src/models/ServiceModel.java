package models;

import controllers.MainMenuController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

public class ServiceModel {

    private LocalDate actualDate;
    private LocalDate serviceStart;
    private LocalDate serviceEnd;
    private ArrayList<String> recurenceDay;
    private ArrayList<ServiceInscriptionModel> inscriptions;
    private String serviceCode;

    private Map<String, String> infos;


    public ServiceModel(LocalDate serviceStart, LocalDate serviceEnd, ArrayList<String> recurenceDay, Map<String, String> infos) {
        this.serviceStart = serviceStart;
        this.serviceEnd = serviceEnd;
        this.recurenceDay = recurenceDay;
        this.infos = infos;
        this.inscriptions = new ArrayList<>();
        this.actualDate =  LocalDate.now();;

        int i = 1000000;
        while(MainMenuController.serviceList.containsKey(""+i)){
            i++;
        }
        this.serviceCode = i+"";
    }

    public ServiceModel(LocalDate serviceStart, LocalDate serviceEnd, ArrayList<String> recurenceDay, Map<String, String> infos, String serviceCode) {
        this.serviceStart = serviceStart;
        this.serviceEnd = serviceEnd;
        this.recurenceDay = recurenceDay;
        this.infos = infos;
        this.inscriptions = new ArrayList<>();
        this.serviceCode = serviceCode;
    }

    public LocalDate getActualDate() {
        return actualDate;
    }

    public LocalDate getServiceStart() {
        return serviceStart;
    }

    public LocalDate getServiceEnd() {
        return serviceEnd;
    }

    public String getServiceHour() {
        return infos.get("serviceHour");
    }

    public ArrayList<String> getRecurenceDay() {
        return recurenceDay;
    }

    public int getCapacity() {
        return Integer.parseInt(infos.get("capacity"));
    }

    public String getProfessionnalId() {
        return infos.get("professionnalId");
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public String getCost() {
        return infos.get("cost");
    }

    public String getCommentary() {
        return infos.get("comments");
    }

    public String getName() {
        return infos.get("name");
    }

    public ArrayList<ServiceInscriptionModel> getInscriptions() {
        return inscriptions;
    }

    public void addInscriptions(ServiceInscriptionModel inscription) {
        this.inscriptions.add(inscription);
    }

    public int getPlaces(){
        return getCapacity() - getInscriptions().size();
    }
}
