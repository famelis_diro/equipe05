package models;

import java.time.LocalDate;

public class ServiceInscriptionModel {

    private String idMembre;
    private String idPro;
    private ServiceModel service;
    private LocalDate actualDate;
    private LocalDate serviceDate;
    private String comments;

    public ServiceInscriptionModel(String idMembre, String idPro, ServiceModel service, LocalDate actualDate, LocalDate serviceDate) {
        this.idMembre = idMembre;
        this.idPro = idPro;
        this.service = service;
        this.actualDate = actualDate;
        this.serviceDate = serviceDate;
        this.comments = "";
    }

    public String getIdMembre() {
        return idMembre;
    }

    public String getIdPro() {
        return idPro;
    }

    public ServiceModel getService() {
        return service;
    }
}
