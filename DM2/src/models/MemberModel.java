package models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class MemberModel extends PersonModel {

    private String paiementMethod;
    private LocalDate paiementDate;

    public MemberModel(String lastName, String firstName, String adress, String city, String telNumber,
                       String favoriteWord, String age, String paiementMethod, String idNumber) {
        super(lastName, firstName, adress, city, telNumber, favoriteWord, age, idNumber);
        this.role = "Membre";
        this.paiementMethod = paiementMethod;
        this.paiementDate = null;
    }

    public MemberModel(String lastName, String firstName, String adress, String city, String telNumber,
                       String favoriteWord, String age, String paiementMethod, String idNumber, LocalDate paiementDate) {
        super(lastName, firstName, adress, city, telNumber, favoriteWord, age, idNumber);
        this.role = "Membre";
        this.paiementMethod = paiementMethod;
        this.paiementDate = paiementDate;
    }

    public MemberModel(String lastName, String firstName, String adress, String city, String telNumber,
                       String favoriteWord, String age, String paiementMethod) {
        super(lastName, firstName, adress, city, telNumber, favoriteWord, age);
        this.role = "Membre";
        this.paiementMethod = paiementMethod;
        this.paiementDate = null;

    }

    public String getPaiementMethod() {
        return paiementMethod;
    }

    public LocalDate getPaiementDate() {
        return paiementDate;
    }

    public String getStatus(){
        if(getPaiementDate() == null) {
            this.status = "Inscription non-complété";
        } else if(LocalDate.now().compareTo(getPaiementDate().plusMonths(1)) == -1){
            this.status = "Membre actif";
        } else {
            this.status = "Membre suspendu";
        }
        return this.status;
    }

    public void setPaiementDate() {
        this.paiementDate = LocalDate.now();
        getStatus();
    }
}
