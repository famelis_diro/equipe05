package models;

import controllers.MainMenuController;

public class PersonModel {

    private String lastName;
    private String firstName;
    private String adress;
    private String city;
    private String telNumber;
    private String favoriteWord;
    private String age;
    private String idNumber;
    public String role;
    public String status;

    public PersonModel(String lastName, String firstName, String adress, String city, String telNumber,
                       String favoriteWord, String age) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.adress = adress;
        this.city = city;
        this.telNumber = telNumber;
        this.favoriteWord = favoriteWord;
        this.age = age;
        this.status = "Inscription non-complété";

        int i = 100000000;
        while(MainMenuController.personList.containsKey(""+i)){
            i++;
        }
        this.idNumber = i+"";
    }

    public PersonModel(String lastName, String firstName, String adress, String city, String telNumber,
                       String favoriteWord, String age, String idNumber) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.adress = adress;
        this.city = city;
        this.telNumber = telNumber;
        this.favoriteWord = favoriteWord;
        this.age = age;
        this.status = "Inactif";
        this.idNumber = idNumber;
    }

    public PersonModel(){

    }

    //Getter
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAdress() { return adress; }

    public String getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public String getFavoriteWord() {
        return favoriteWord;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public String getRole() {
        return role;
    }

    public String getStatus() { return status; }

    //Setter
    public void setStatus(String status) { this.status = status; }
}
