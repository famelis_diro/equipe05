package models;

import java.util.HashMap;
import java.util.Map;

public class TEFModel {

    public String idPro;
    public String firstName;
    public String lastName;
    public Map<ServiceModel, Integer> serviceList = new HashMap<>();

    public TEFModel(String idPro, String firstName, String lastName) {
        this.idPro = idPro;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void addService(ServiceModel service){
        int numberOfInscription = service.getInscriptions().size();
        serviceList.put(service, numberOfInscription);
    }

    public String getIdPro() {
        return idPro;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Map<ServiceModel, Integer> getServiceList() {
        return serviceList;
    }
}
