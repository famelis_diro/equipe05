package ProcedureComptable;

import controllers.MainMenuController;
import models.PersonModel;
import models.ServiceInscriptionModel;
import models.ServiceModel;
import models.TEFModel;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashMap;
import java.util.Map;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ProcedureComptable {



    public static Map<String, TEFModel> tefList(){
        LocalDate today = LocalDate.now().with(DayOfWeek.FRIDAY);
        LocalDate lastFriday = today.minus(Period.ofDays(7));
        Map<String, TEFModel> tefList = new HashMap<>();

        for(Map.Entry<String, ServiceModel> service : MainMenuController.serviceList.entrySet()){
            LocalDate start = service.getValue().getServiceStart();
            LocalDate end = service.getValue().getServiceEnd();

            //Extract this week services
            if(today.compareTo(start) >= 0 && lastFriday.compareTo(end) <= 0){
                String idPro = service.getValue().getProfessionnalId();

                if(tefList.containsKey(idPro)){
                    tefList.get(idPro).addService(service.getValue());
                } else {
                    PersonModel pro = MainMenuController.personList.get(idPro);
                    TEFModel tef = new TEFModel(idPro, pro.getFirstName(), pro.getLastName());
                    tef.addService(service.getValue());
                    tefList.put(idPro, tef);
                }
            }
        }
        return tefList;
    }

    public static void generateReport() throws IOException {
        Map<String, TEFModel> tefList = tefList();
        String report = "";
        Double totalPrice = 0.0;
        int servicesCount = 0;
        int prosCount = 0;

        for(Map.Entry<String, TEFModel> tef : tefList.entrySet()){
            TEFModel tefExtract = tef.getValue();
            report += "Id du professionnel: "+tefExtract.idPro+", "+
                    "Nombre de service: "+tefExtract.serviceList.size()+", "+
                    "Frais total: "+getPrice(tefExtract) +  "\n\n";
            servicesCount += tefExtract.serviceList.size();
            totalPrice += getPrice(tefExtract);
            prosCount++;
        }
        report += "\n \nPrix Total: " + totalPrice +"\n"+
                    "Nombre total de services: "+servicesCount+"\n"+
                    "Nombre total de professionnels: "+prosCount;

        FileWriter writer = new FileWriter("rapport.txt");
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write(report);
        bw.close();

    }

    public static void generateTEF() throws IOException {
        Map<String, TEFModel> tefList = tefList();
        String tefFile = "";

        for(Map.Entry<String,TEFModel> tef : tefList.entrySet()){
            tefFile += "Professionnel: "+tef.getValue().getFirstName()+" "+tef.getValue().getLastName()+
                    ", ID: "+tef.getValue().getIdPro()+", Services:\n";
            for(Map.Entry<ServiceModel, Integer> serviceModel : tef.getValue().getServiceList().entrySet()){
                tefFile += serviceModel.getKey().getName() + " "+serviceModel.getValue()+" membres inscrits";
            }tefFile += "\n\n";
        }


        FileWriter writer = new FileWriter("rapport.txt");
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write(tefFile);
        bw.close();
    }

    private static double getPrice(TEFModel tef){
        double price = 0;
        for(Map.Entry<ServiceModel, Integer> service : tef.serviceList.entrySet()){
            price += (Double.parseDouble(service.getKey().getCost()))*service.getValue();
        }
        return price;
    }






}
