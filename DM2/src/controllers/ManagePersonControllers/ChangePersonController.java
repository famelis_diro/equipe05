package controllers.ManagePersonControllers;

import controllers.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import models.MemberModel;
import models.PersonModel;
import models.ProfessionnalModel;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ChangePersonController {

    @FXML
    private Button changeConfirmationButton;
    @FXML
    private TextField idNumberHidden;
    @FXML
    private TextField lastName;
    @FXML
    private TextField firstName;
    @FXML
    private TextField adress;
    @FXML
    private TextField city;
    @FXML
    private TextField telNumber;
    @FXML
    private TextField favoriteWord;
    @FXML
    private ChoiceBox ageChoice;
    @FXML
    private ChoiceBox paiementMethod;
    @FXML
    private TextField searchField;

    public void onChangeConfirmationSelect(MouseEvent mouseEvent) throws IOException {
        MemberModel newMember = null;
        ProfessionnalModel newPro = null;

        String id = idNumberHidden.getText();

        //Member inscription
        if(MainMenuController.personList.get(id).getRole().equals("Membre")){
            //TODO: Ajouter la vérification des champs

            MemberModel member = (MemberModel) MainMenuController.personList.get(id);

            newMember = new MemberModel(lastName.getText(), firstName.getText(), adress.getText(), city.getText(), telNumber.getText(),
                    favoriteWord.getText(), (String) ageChoice.getSelectionModel().getSelectedItem(),
                    (String) paiementMethod.getSelectionModel().getSelectedItem(), id, member.getPaiementDate());

            MainMenuController.personList.put(id, newMember);
            showMemberProfil(MainMenuController.mainPage, newMember);
        }
        //Professionnal inscription
        else if(MainMenuController.personList.get(id).getRole().equals("Professionnel")){
            //TODO: Ajouter la vérification des champs sinon dataNotValid
            //TODO: Masquer choix de méthode de paiement pour un pro

            newPro = new ProfessionnalModel(lastName.getText(), firstName.getText(), adress.getText(), city.getText(), telNumber.getText(),
                    favoriteWord.getText(), (String) ageChoice.getSelectionModel().getSelectedItem(), id);

            MainMenuController.personList.put(id, newPro);
            showProfessionnalProfil(MainMenuController.mainPage, newPro);
        }
    }


    public void onSearchSelect(MouseEvent mouseEvent) throws IOException {
        String id = searchField.getText();
        Stage search = (Stage) searchField.getScene().getWindow();

        if(MainMenuController.personList.containsKey(id)){
            PersonModel personModel = MainMenuController.personList.get(id);
            if(personModel.getRole().equals("Membre")){
                search.close();
                showMemberProfil(MainMenuController.mainPage, personModel);
            } else{
                search.close();
                showProfessionnalProfil(MainMenuController.mainPage, personModel);
            }

        }else{
            Parent root = FXMLLoader.load(getClass().getResource("/views/popUpWindows/searchView.fxml"));

            search.setScene(new Scene(root, 600, 400));
            search.show();
        }

    }

    public void showMemberProfil(Stage stage, PersonModel person) throws IOException {

        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/PersonViews/memberView.fxml"));

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        MemberModel member = (MemberModel) person;

        view.getNamespace().putAll(getData(person));
        if(member.getPaiementDate() != null)
            view.getNamespace().put("paiementDate", dtf.format(member.getPaiementDate()));
        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();

    }

    public void showProfessionnalProfil(Stage stage, PersonModel person) throws IOException {

        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/PersonViews/professionnalView.fxml"));

        view.getNamespace().putAll(getData(person));
        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();

    }

    private Map<String, String> getData(PersonModel person){
        Map<String,String> data = new HashMap<String, String>(){{
            put("firstName",person.getFirstName());
            put("lastName", person.getLastName());
            put("adress", person.getAdress());
            put("city", person.getCity());
            put("telNumber", person.getTelNumber());
            put("favoriteWord", person.getFavoriteWord());
            put("status", person.getStatus());
            put("age", person.getAge());
            put("id", person.getIdNumber());
        }};

        return data;
    }

    public void backToMenu(MouseEvent mouseEvent) throws IOException {
        Stage stage = (Stage) changeConfirmationButton.getScene().getWindow();

        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }
}
