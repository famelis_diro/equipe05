package controllers.ManagePersonControllers;

import controllers.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.MemberModel;
import models.PersonModel;
import models.ProfessionnalModel;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class InscriptionController {

    @FXML
    private Button confirmationButton;
    @FXML
    private TextField lastName;
    @FXML
    private TextField firstName;
    @FXML
    private TextField adress;
    @FXML
    private TextField city;
    @FXML
    private TextField telNumber;
    @FXML
    private TextField favoriteWord;
    @FXML
    private CheckBox memberCheck;
    @FXML
    private CheckBox professionnalCheck;
    @FXML
    private ChoiceBox ageChoice;
    @FXML
    private ChoiceBox paiementMethod;

    /**
     * When "Confirmer" BUTTON is selected
     * Create a new instance of MemberModel or ProfessionnalModel
     * @throws IOException
     */
    public void onConfirmationSelect() throws IOException {
        Stage stage = MainMenuController.mainPage;

        //Member inscription
       if(memberCheck.isSelected() && !professionnalCheck.isSelected()){
           MemberModel newMember = new MemberModel(lastName.getText(), firstName.getText(), adress.getText(), city.getText(), telNumber.getText(),
                   favoriteWord.getText(), (String) ageChoice.getSelectionModel().getSelectedItem(),
                   (String) paiementMethod.getSelectionModel().getSelectedItem());

           MainMenuController.personList.put(newMember.getIdNumber(), newMember);
           showMemberProfil(stage, newMember);
       }
       //Professionnal inscription
       else if(!memberCheck.isSelected() && professionnalCheck.isSelected()){
           ProfessionnalModel newPro = new ProfessionnalModel(lastName.getText(), firstName.getText(), adress.getText(), city.getText(), telNumber.getText(),
                   favoriteWord.getText(), (String) ageChoice.getSelectionModel().getSelectedItem());

           MainMenuController.personList.put(newPro.getIdNumber(), newPro);
           showProfessionnalProfil(stage, newPro);
       } else {
           Parent root = FXMLLoader.load(getClass().getResource("/views/PersonViews/inscriptionView.fxml"));

           Scene scene = new Scene(root);
           stage.setScene(scene);
           MainMenuController.mainPage = stage;
           stage.show();
       }
    }

    /**
     * Show a member profil window
     * @param stage
     * @param person
     * @throws IOException
     */
    public void showMemberProfil(Stage stage, PersonModel person) throws IOException {

        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/PersonViews/memberView.fxml"));

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        MemberModel member = (MemberModel) person;

        view.getNamespace().putAll(getData(person));
        if(member.getPaiementDate() != null)
            view.getNamespace().put("paiementDate", dtf.format(member.getPaiementDate()));
        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();

    }

    /**
     * Show a professionnal profil window
     * @param stage
     * @param person
     * @throws IOException
     */
    public void showProfessionnalProfil(Stage stage, PersonModel person) throws IOException {

        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/PersonViews/professionnalView.fxml"));

        view.getNamespace().putAll(getData(person));
        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();

    }

    /**
     * Back to main menu
     * @throws IOException
     */
    public void backToMenu() throws IOException {
        Stage stage = (Stage) confirmationButton.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();

    }

    /**
     * Get the data for the person
     * @param person
     * @return data
     */
    private Map<String, String> getData(PersonModel person){
        Map<String,String> data = new HashMap<String, String>(){{
            put("firstName",person.getFirstName());
            put("lastName", person.getLastName());
            put("adress", person.getAdress());
            put("city", person.getCity());
            put("telNumber", person.getTelNumber());
            put("favoriteWord", person.getFavoriteWord());
            put("status", person.getStatus());
            put("age", person.getAge());
            put("id", person.getIdNumber());
        }};

        return data;
    }
}
