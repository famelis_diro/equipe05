package controllers.ManagePersonControllers;

import controllers.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import models.MemberModel;
import models.PersonModel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ShowProfilController {

    @FXML
    private Label idNumber;

    public static String idPro;

    public void onDeleteSelect() throws IOException {
        String id = idNumber.getText();
        MainMenuController.personList.remove(id);
        backToMenu();
    }

    public void onChangeSelect() throws IOException {
        Stage stage = MainMenuController.mainPage;
        String id = idNumber.getText();

        PersonModel person = MainMenuController.personList.get(id);

        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/PersonViews/changeView.fxml"));

        view.getNamespace().putAll(getData(person));

        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    public void backToMenu() throws IOException {
        Stage stage = MainMenuController.mainPage;

        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();

    }

    public void onConsultServicesSelect() throws IOException {
        Stage stage = MainMenuController.mainPage;
        idPro = idNumber.getText();

        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/PersonViews/inscriptionListView.fxml"));

        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    private Map<String, String> getData(PersonModel person) {
        Map<String, String> data = new HashMap<String, String>() {{
            put("getFirstName", person.getFirstName());
            put("getLastName", person.getLastName());
            put("getAdress", person.getAdress());
            put("getCity", person.getCity());
            put("getTelNumber", person.getTelNumber());
            put("getFavoriteWord", person.getFavoriteWord());
            put("getAge", person.getAge());
            put("getIdNumber", person.getIdNumber());
        }};

        if(person.getRole().equals("Membre")) {
            MemberModel member = (MemberModel) person;
            data.put("getPaiementMethod", member.getPaiementMethod());
        }

        return data;
    }

    public void onPaiementCompleteSelect() throws IOException {
        String id = idNumber.getText();
        MemberModel member = (MemberModel) MainMenuController.personList.get(id);

        member.setStatus("Membre actif");
        member.setPaiementDate();

        backToMenu();
    }
}
