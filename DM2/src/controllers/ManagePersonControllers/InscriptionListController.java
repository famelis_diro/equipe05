package controllers.ManagePersonControllers;

import controllers.MainMenuController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import models.PersonModel;
import models.ServiceInscriptionModel;
import models.ServiceModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InscriptionListController {

    @FXML
    private ListView list;


    /**
     * Initialize Inscription List view
     */
    @FXML
    public void initialize() {
        List<String> values = new ArrayList<>();
        for(Map.Entry<String, ServiceModel> service : MainMenuController.serviceList.entrySet()){

            if(service.getValue().getProfessionnalId().equals(ShowProfilController.idPro)) {
                values.add(service.getValue().getName() + ": ");
                for (ServiceInscriptionModel inscription : service.getValue().getInscriptions()) {
                    PersonModel person = MainMenuController.personList.get(inscription.getIdMembre());
                    values.add(person.getFirstName() + " " + person.getLastName());
                }
            }

        }
        list.setItems(FXCollections.observableList(values));
    }

    /**
     * Back to main menu
     * @throws IOException
     */
    public void backToMenu() throws IOException {
        Stage stage = MainMenuController.mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }
}
