package controllers;

import ProcedureComptable.ProcedureComptable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import models.PersonModel;
import models.ServiceInscriptionModel;
import models.ServiceModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainMenuController {

    @FXML
    private TextField searchField;
    @FXML
    private Button okButton;

    //List to save all person, services and inscriptions
    public static Map<String, PersonModel> personList = new HashMap<>();
    public static Map<String, ServiceModel> serviceList = new HashMap<>();
    public static ArrayList<ServiceInscriptionModel> serviceInscriptionList = new ArrayList<>();

    //Stages of views
    public static Stage mainPage;
    public static Stage validationPage;

    /**
     * When "Inscription" BUTTON is selected from MainView
     * Show the inscription window
     * @throws IOException
     */
    public void onInscriptionSelect() throws IOException {
        Stage stage = mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/PersonViews/inscriptionView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        mainPage = stage;
        stage.show();
    }

    /**
     * When "Gérer un compte" BUTTON is selected from MainView
     * Show a search pop-up window
     * @throws IOException
     */
    public void onManageSelect() throws IOException {
        // Create Window
        Parent root = FXMLLoader.load(getClass().getResource("/views/popUpWindows/searchView.fxml"));
        Stage searchStage = new Stage();

        searchStage.setTitle("IFT2255 - TP2");
        searchStage.setScene(new Scene(root, 600, 400));
        searchStage.setMinHeight(400);
        searchStage.setMinWidth(600);
        searchStage.show();
    }

    /**
     *When "Valider un membre" BUTTON is selected from MainView
     * Show a validation pop-up window
     * @throws IOException
     */
    public void onValidationSearchSelect() throws IOException {
        // Create Window
        Parent root = FXMLLoader.load(getClass().getResource("/views/popUpWindows/validationView.fxml"));
        Stage validation = new Stage();

        validation.setTitle("IFT2255 - TP2");
        validation.setScene(new Scene(root, 600, 400));
        validation.setMinHeight(400);
        validation.setMinWidth(600);
        validationPage = validation;
        validation.show();
    }

    /**
     * When "Services" BUTTON is selected from MainView
     * Show the services option window
     * @param mouseEvent
     * @throws IOException
     */
    public void onServicesSelect(MouseEvent mouseEvent) throws IOException {
        Stage stage = mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/ServicesViews/servicesOptionView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        mainPage = stage;
        stage.show();
    }

    /**
     * When "Générer rapport" BUTTON is selected from MainView
     * Produce "rapport.txt" file
     * @throws IOException
     */
    public void onReportSelect() throws IOException {
        ProcedureComptable.generateReport();
    }

    /**
     *When "Valider" BUTTON is selected from validationView
     *Show the member status ("Validé", "Membre suspendu", "inscription non-complété")
     * @throws IOException
     */
    public void onValidationSelect(MouseEvent mouseEvent) throws IOException {
        String id = searchField.getText();
        validationPage.close();

        if(MainMenuController.personList.containsKey(id)){
            PersonModel personModel = MainMenuController.personList.get(id);
            if(personModel.getRole().equals("Membre")){
                switch (personModel.getStatus()){
                    case "Membre actif":
                        showMessage("Validé");
                        break;
                    case "Membre suspendu":
                        showMessage("Membre suspendu");
                        break;
                    case "Inscription non-complété":
                        showMessage("Le premier paiement n'a pas été complété");
                        break;
                }
            } else if(personModel.getRole().equals("Professionnel")){
                showMessage("Ce numéro appartient à un professionnel");
            }
        } else { showMessage("Numéro invalide");}
    }


    /**
     * Show a message pop-up window
     * @param message
     * @throws IOException
     */
    public void showMessage(String message) throws IOException {
        // Create Window
        FXMLLoader messageView = new FXMLLoader(getClass().getResource("/views/popUpWindows/messageView.fxml"));
        messageView.getNamespace().put("message", message);

        Parent root = messageView.load();
        Stage messageStage = new Stage();

        messageStage.setTitle("IFT2255 - TP2");
        messageStage.setScene(new Scene(root, 600, 400));
        messageStage.setMinHeight(400);
        messageStage.setMinWidth(600);
        messageStage.show();

    }

    /**
     * When "Ok" BUTTON is selected from pop-up window
     * Close the pop-up window and go back to the present page
     */
    public void onOkSelect() {
        Stage present = (Stage) okButton.getScene().getWindow();
        present.close();
    }
}
