package controllers.ManageServiceControllers;

import controllers.MainMenuController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class ServicesOptionController {

    /**
     * When service list BUTTON is selected
     * Show a list of services
     * @throws IOException
     */
    public void onServicesListSelect() throws IOException {
        Stage stage = MainMenuController.mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/ServicesViews/serviceListView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    /**
     * When create service BUTTON is selected
     * the create service window is shown
     * @throws IOException
     */
    public void onCreateServiceSelect() throws IOException {
        Stage stage = MainMenuController.mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/ServicesViews/createServiceView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    /**
     * Back to the main menu
     * @throws IOException
     */
    public void backToMenu() throws IOException {
        Stage stage = MainMenuController.mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    /**
     * When the manage service BUTTON is selected
     * Show a search pop-up window for services
     * @throws IOException
     */
    public void onManageSelect() throws IOException {
        // Create Window
        Parent root = FXMLLoader.load(getClass().getResource("/views/popUpWindows/searchServiceView.fxml"));
        Stage searchStage = new Stage();

        searchStage.setTitle("IFT2255 - TP2");
        searchStage.setScene(new Scene(root, 600, 400));
        searchStage.setMinHeight(400);
        searchStage.setMinWidth(600);
        searchStage.show();
    }

}
