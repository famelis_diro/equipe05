package controllers.ManageServiceControllers;

import controllers.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import models.ServiceModel;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreateServiceController {
    @FXML
    private TextField serviceName;
    @FXML
    private TextField idProf;
    @FXML
    private TextField price;
    @FXML
    private TextField capacity;
    @FXML
    private TextField hour;
    @FXML
    private TextArea commentsArea;
    @FXML
    private DatePicker startingDate;
    @FXML
    private DatePicker endingDate;

    @FXML
    private CheckBox monday;
    @FXML
    private CheckBox tuesday;
    @FXML
    private CheckBox wednesday;
    @FXML
    private CheckBox thursday;
    @FXML
    private CheckBox friday;
    @FXML
    private CheckBox saturday;
    @FXML
    private CheckBox sunday;

    /**
     * Create a service when the confirmation BUTTON is selected
     * @throws IOException
     */
    public void onConfirmationSelect() throws IOException {
        String idProfNumber = idProf.getText();


        if(startingDate.getValue() == null || endingDate.getValue() == null || capacity.getText() == null || serviceName == null)
            showMessage("Les champs ne sont pas complets");
        else if(!MainMenuController.personList.containsKey(idProfNumber) ||
                (MainMenuController.personList.containsKey(idProfNumber) &&
                        !MainMenuController.personList.get(idProfNumber).getRole().equals("Professionnel")))
            showMessage("L'id du prof n'est pas valide");
        else {

            ServiceModel newService = new ServiceModel(startingDate.getValue(), endingDate.getValue(),
                    setDays(), getInfos());
            MainMenuController.serviceList.put(newService.getServiceCode(), newService);

            showService(newService);
        }
    }

    /**
     * Back to main menu
     * @throws IOException
     */
    public void backToMenu() throws IOException {
        Stage stage = MainMenuController.mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    /**
     * Set selected days of week for the service
     * @return
     */
    public ArrayList<String> setDays(){
        ArrayList<String> days = new ArrayList<>();

        if(monday.isSelected())days.add("Lundi");
        if(tuesday.isSelected())days.add("Mardi");
        if(wednesday.isSelected())days.add("Mercredi");
        if(thursday.isSelected())days.add("Jeudi");
        if(friday.isSelected())days.add("Vendredi");
        if(saturday.isSelected())days.add("Samedi");
        if(sunday.isSelected())days.add("Dimanche");

        return days;
    }

    /**
     * Show a pop-up window with a message
     * @param message
     * @throws IOException
     */
    public void showMessage(String message) throws IOException {
        // Create Window
        FXMLLoader messageView = new FXMLLoader(getClass().getResource("/views/popUpWindows/messageView.fxml"));
        messageView.getNamespace().put("message", message);

        Parent root = messageView.load();
        Stage messageStage = new Stage();

        messageStage.setTitle("IFT2255 - TP2");
        messageStage.setScene(new Scene(root, 600, 400));
        messageStage.setMinHeight(400);
        messageStage.setMinWidth(600);
        messageStage.show();

    }

    /**
     * Show a window with the service information
     * @param newService
     * @throws IOException
     */
    public void showService(ServiceModel newService) throws IOException {
        Stage stage = MainMenuController.mainPage;
        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/ServicesViews/serviceDetailsView.fxml"));
        ShowServiceController.serviceCodeSelected = newService.getServiceCode();
        view.getNamespace().putAll(getData(newService));
        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();

    }

    /**
     * Get the info from the text field on the window
     * @return
     */
    private Map<String, String> getInfos(){
        Map<String, String> infos = new HashMap<String, String>(){{
            put("capacity", capacity.getText());
            put("serviceHour", hour.getText());
            put("professionnalId", idProf.getText());
            put("cost", price.getText());
            put("comments", commentsArea.getText());
            put("name", serviceName.getText());
        }};

        return infos;
    }

    /**
     * Get the data from the service
     * @param service
     * @return
     */
    private Map<String, String> getData(ServiceModel service) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        Map<String, String> data = new HashMap<String, String>() {{
            put("getServiceCode", service.getServiceCode());
            put("getIdPro", service.getProfessionnalId());
            put("getCost", service.getCost());
            put("getCapacity", service.getCapacity() + "");
            put("getHour", service.getServiceHour());
            put("getPlaces", service.getPlaces() + "");
            put("getComments", service.getCommentary());
            put("getStartingDate", dtf.format(service.getServiceStart()));
            put("getEndingDate", dtf.format(service.getServiceEnd()));
            put("getName", service.getName());
        }};

        String days = "";
        for (String day : service.getRecurenceDay()) {
            days += day;
            if (service.getRecurenceDay().indexOf(day) != service.getRecurenceDay().size() - 1)
                days += ", ";
        }
        data.put("getDays", days);

        return data;
    }
}
