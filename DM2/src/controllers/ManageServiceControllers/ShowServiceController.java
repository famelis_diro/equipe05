package controllers.ManageServiceControllers;

import controllers.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import models.ServiceConfirmationModel;
import models.ServiceInscriptionModel;
import models.ServiceModel;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;


public class ShowServiceController {
    @FXML
    private Label serviceCode;
    @FXML
    private TextField searchField;

    private Stage validationPage;
    public static String serviceCodeSelected;

    public void backToMenu() throws IOException {
        Stage stage = MainMenuController.mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    public void onDeleteSelect() throws IOException {
        MainMenuController.serviceList.remove(serviceCode.getText());
        backToMenu();
    }

    public void onChangeSelect(MouseEvent mouseEvent) throws IOException {
        Stage stage = MainMenuController.mainPage;
        String id = serviceCode.getText();
        serviceCodeSelected = serviceCode.getText();

        ServiceModel service = MainMenuController.serviceList.get(id);

        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/ServicesViews/changeServiceView.fxml"));

        view.getNamespace().putAll(getData(service));

        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    private Map<String, String> getData(ServiceModel service) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        Map<String, String> data = new HashMap<String, String>() {{
            put("getIdNumber", service.getServiceCode());
            put("getIdProf", service.getProfessionnalId());
            put("getPrice", service.getCost());
            put("getCapacity", service.getCapacity() + "");
            put("getHour", service.getServiceHour());
            put("getComments", service.getCommentary());
            put("getName", service.getName());
        }};

        return data;
    }

    public void onConfirmInscriptionSelect() throws IOException {
        serviceCodeSelected = serviceCode.getText();
        Parent root = FXMLLoader.load(getClass().getResource("/views/popUpWindows/confirmInscriptionView.fxml"));
        Stage validation = new Stage();


        validation.setTitle("IFT2255 - TP2");
        validation.setScene(new Scene(root, 600, 400));
        validation.setMinHeight(400);
        validation.setMinWidth(600);
        validationPage = validation;
        validation.show();
    }

    public void onInscriptionConfirmationSelectEnd() throws IOException {
        boolean found = false;
        for(ServiceInscriptionModel inscription : MainMenuController.serviceInscriptionList){
            if(inscription.getIdMembre().equals(searchField.getText()) &&
                    inscription.getService().getServiceCode().equals(serviceCodeSelected)) {
                ServiceConfirmationModel newConfirmation = new ServiceConfirmationModel(LocalDate.now(), inscription.getIdPro(),
                        inscription.getIdMembre(), inscription.getService().getServiceCode());
                showMessage("Validé");
                found = true;
                break;
            }
        }
        if(!found)
            showMessage("Non-valide");
    }

    public void showMessage(String message) throws IOException {
        // Create Window
        FXMLLoader messageView = new FXMLLoader(getClass().getResource("/views/popUpWindows/messageView.fxml"));
        messageView.getNamespace().put("message", message);

        Parent root = messageView.load();
        Stage messageStage = new Stage();

        messageStage.setTitle("IFT2255 - TP2");
        messageStage.setScene(new Scene(root, 600, 400));
        messageStage.setMinHeight(400);
        messageStage.setMinWidth(600);
        messageStage.show();
    }

}
