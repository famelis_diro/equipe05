package controllers.ManageServiceControllers;

import controllers.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.PersonModel;
import models.ServiceInscriptionModel;
import models.ServiceModel;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.time.LocalDate;

public class ServiceInscriptionController {
    @FXML
    private TextField searchField;

    /**
     * When confirming an inscription to a service
     * @throws IOException
     */
    public void onInscriptionConfirmationSelect() throws IOException {
        String id = searchField.getText();
        ServiceListController.validationPage.close();

        if(MainMenuController.personList.containsKey(id)){
            PersonModel personModel = MainMenuController.personList.get(id);
            if(personModel.getRole().equals("Membre")){
                switch (personModel.getStatus()){
                    case "Membre actif":
                        ServiceModel service = MainMenuController.serviceList.get(ServiceListController.saveServiceCode);
                        createInscription(personModel, service);
                        showMessage("Le montant à payer est de "+
                                service.getCost()+"$");
                        break;
                    case "Membre suspendu":
                        showMessage("Membre suspendu, inscription non-complété");
                        break;
                    case "Inscription non-complété":
                        showMessage("Le premier paiement n'a pas été complété, inscription non-complété");
                        break;
                }
            } else if(personModel.getRole().equals("Professionnel")){
                showMessage("Ce numéro appartient à un professionnel");
            }
        } else { showMessage("Numéro invalide");}
    }

    /**
     * Create an inscription
     * @param personModel
     * @param service
     */
    private void createInscription(PersonModel personModel, ServiceModel service) {
        ServiceInscriptionModel newServiceInscription = new ServiceInscriptionModel(personModel.getIdNumber(), service.getProfessionnalId(),
                service, LocalDate.now(), service.getServiceStart());
        MainMenuController.serviceInscriptionList.add(newServiceInscription);
        service.addInscriptions(newServiceInscription);
    }

    /**
     * Show a pop-up view with a message
     * @param message
     * @throws IOException
     */
    public void showMessage(String message) throws IOException {
        // Create Window
        FXMLLoader messageView = new FXMLLoader(getClass().getResource("/views/popUpWindows/messageView.fxml"));
        messageView.getNamespace().put("message", message);

        Parent root = messageView.load();
        Stage messageStage = new Stage();

        messageStage.setTitle("IFT2255 - TP2");
        messageStage.setScene(new Scene(root, 600, 400));
        messageStage.setMinHeight(400);
        messageStage.setMinWidth(600);
        messageStage.show();
    }
}
