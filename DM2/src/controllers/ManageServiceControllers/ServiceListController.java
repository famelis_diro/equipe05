package controllers.ManageServiceControllers;

import controllers.MainMenuController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import models.ServiceModel;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ServiceListController {

    @FXML
    private ListView list;

    public static String saveServiceCode;
    public static Stage validationPage;

    /**
     * Initialize the window
     */
    @FXML
    public void initialize() {
        List<String> values = new ArrayList<>();
        for(Map.Entry<String, ServiceModel> service : MainMenuController.serviceList.entrySet()){
            values.add("Nom du service: "+service.getValue().getName()+"          "
                    +"Places disponibles: "+service.getValue().getPlaces() +"          "+
                    "Code de service: "+service.getValue().getServiceCode());
        }

        list.setItems(FXCollections.observableList(values));
    }

    /**
     * Back to main menu
     * @throws IOException
     */
    public void backToMenu() throws IOException {
        Stage stage = MainMenuController.mainPage;
        Parent root = FXMLLoader.load(getClass().getResource("/views/mainView.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    /**
     * Show a windows with Service details
     * @throws IOException
     */
    public void showDetails() throws IOException {
        String serviceCode = getServiceCode();

        if (serviceCode != null) {
            showService(MainMenuController.serviceList.get(serviceCode));
        } else { showMessage("Aucune sélection");}
    }

    /**
     * Get service code of the selected service
     * @return
     */
    public String getServiceCode(){
        Object selection = list.getSelectionModel().getSelectedItem();

        if (selection != null) {
            String selectionString = (String) selection;
            String serviceCode = selectionString.split(":")[3].trim();
            return serviceCode;
        } return null;
    }

    /**
     * Show a service window
     * @param newService
     * @throws IOException
     */
    public void showService(ServiceModel newService) throws IOException {
        Stage stage = MainMenuController.mainPage;
        FXMLLoader view = new FXMLLoader(getClass().getResource("/views/ServicesViews/serviceDetailsView.fxml"));
        ShowServiceController.serviceCodeSelected = newService.getServiceCode();

        view.getNamespace().putAll(getData(newService));
        Parent root = view.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        MainMenuController.mainPage = stage;
        stage.show();
    }

    /**
     * Get the data of a service
     * @param service
     * @return
     */
    private Map<String, String> getData(ServiceModel service) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        Map<String, String> data = new HashMap<String, String>() {{
            put("getServiceCode", service.getServiceCode());
            put("getIdPro", service.getProfessionnalId());
            put("getCost", service.getCost());
            put("getCapacity", service.getCapacity() + "");
            put("getHour", service.getServiceHour());
            put("getPlaces", service.getPlaces() + "");
            put("getComments", service.getCommentary());
            put("getStartingDate", dtf.format(service.getServiceStart()));
            put("getEndingDate", dtf.format(service.getServiceEnd()));
            put("getName", service.getName());
        }};

        String days = "";
        for (String day : service.getRecurenceDay()) {
            days += day;
            if (service.getRecurenceDay().indexOf(day) != service.getRecurenceDay().size() - 1)
                days += ", ";
        }
        data.put("getDays", days);

        return data;
    }

    /**
     * Show a pop-up window to confirm inscription
     * @throws IOException
     */
    public void inscription() throws IOException {
        saveServiceCode = getServiceCode();
        Parent root = FXMLLoader.load(getClass().getResource("/views/popUpWindows/confirmInscriptionCreationView.fxml"));
        Stage validation = new Stage();

        validation.setTitle("IFT2255 - TP2");
        validation.setScene(new Scene(root, 600, 400));
        validation.setMinHeight(400);
        validation.setMinWidth(600);
        validationPage = validation;
        validation.show();
    }

    /**
     * Show a pop-up window with a message
     * @param message
     * @throws IOException
     */
    public void showMessage(String message) throws IOException {
        // Create Window
        FXMLLoader messageView = new FXMLLoader(getClass().getResource("/views/popUpWindows/messageView.fxml"));
        messageView.getNamespace().put("message", message);

        Parent root = messageView.load();
        Stage messageStage = new Stage();

        messageStage.setTitle("IFT2255 - TP2");
        messageStage.setScene(new Scene(root, 600, 400));
        messageStage.setMinHeight(400);
        messageStage.setMinWidth(600);
        messageStage.show();
    }
}
