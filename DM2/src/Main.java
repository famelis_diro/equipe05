import controllers.MainMenuController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.PersonModel;
import models.ServiceModel;

import java.util.Map;

public class Main extends Application {

    private static int width = 900;
    private static int height = 600;

    @Override
    public void start(Stage primaryStage) throws Exception{


        // Create Window
        Parent root = FXMLLoader.load(getClass().getResource("views/mainView.fxml"));
        primaryStage.setTitle("IFT2255 - TP2");
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.setMinHeight(height);
        primaryStage.setMinWidth(width);
        MainMenuController.mainPage = primaryStage;
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
